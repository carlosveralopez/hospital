package mariadb;

import java.sql.*;

public class DBconexion {
	
	protected static Connection conexion=null;
	
	private static String host="localhost";
	    private static String db="universidad";
	    private static String user="root";
	    private static String password="";
	    private static String charset="utf8mb4";

	    public DBconexion(){
	       
	    }

	public static void main(String[] args) throws SQLException{
	    
	        try{
	        	Class.forName("org.mariadb.jdbc.Driver");
	            conexion =  DriverManager.getConnection("jdbc:mysql://"+host+"/"+db,user,password);
	        

	            if (conexion != null) {
					System.out.println("Conexi�n a base de datos " + db + " OK");
				}

			} catch (ClassNotFoundException e) {
				System.out.println(e.getMessage());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			} finally {
				if (conexion != null)
					conexion.close();
			}
		}

	}


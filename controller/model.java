package mariadb;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.*;
public class model extends DBconexion{

	private DBconexion con;
	

	public void connectWithDB() throws SQLException {
	try{
	  
	   Statement stmt = conexion.createStatement();
	   String strSelect = "Select * FROM Medico";
	   System.out.println("El resultado de la consulta es: " + strSelect);
	   System.out.println();
	   ResultSet rSet = stmt.executeQuery(strSelect);
	   convertirCsv(rSet);
	  }catch (Exception exc){
	   exc.printStackTrace();
	  }
	}

	public static void convertirCsv(ResultSet rs) throws SQLException, FileNotFoundException {
	   System.out.println("Convertir a CSV");
	   PrintWriter csvWriter = new PrintWriter(new File("csv/consulta.csv")) ;
	   ResultSetMetaData meta = rs.getMetaData() ;
	   int ncolumnas = meta.getColumnCount() ;
	   String headers = "\"" + meta.getColumnName(1) + "\"" ;
	   for (int i = 2 ; i < ncolumnas + 1 ; i ++ ) {
	      headers += ",\"" + meta.getColumnName(i) + "\"" ;
	  }
	   csvWriter.println(headers) ;
	   while (rs.next()) {
	   String row = "\"" + rs.getString(1) + "\"" ;
	   for (int i = 2 ; i < ncolumnas + 1 ; i ++ ) {
	   row += ",\"" + rs.getString(i) + "\"" ;
	  }
	csvWriter.println(row) ;
	}
	csvWriter.close();
	}

	}


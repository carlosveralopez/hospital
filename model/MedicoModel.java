package mariadb;

import java.util.Date;

public class MedicoModel extends model{
	private int id;
	private char numero_colegiado;
	private char dni;
	private char nombre;
	private char apellido1;
	private char apellido2;
	private int telefono;
	private boolean sexo;
	private java.sql.Date created_at;
	private java.sql.Date updated_at;
	private int user_id;
	private int horario_id;
	private int especialidad_id;

	public MedicoModel(){
		setId(id);
		setApellido1(apellido1);
		setApellido2(apellido2);
		setDni(dni);
		setCreated_at(created_at);
		setEspecialidad_id(especialidad_id);
		setHorario_id(horario_id);
		setNombre(nombre);
		setNumero_colegiado(numero_colegiado);
		setSexo(sexo);
		setTelefono(telefono);
		setUser_id(user_id);
		setUpdated_at(updated_at);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public char getNumero_colegiado() {
		return numero_colegiado;
	}

	public void setNumero_colegiado(char numero_colegiado) {
		this.numero_colegiado = numero_colegiado;
	}

	public char getDni() {
		return dni;
	}

	public void setDni(char dni) {
		this.dni = dni;
	}

	public char getNombre() {
		return nombre;
	}

	public void setNombre(char nombre) {
		this.nombre = nombre;
	}

	public char getApellido1() {
		return apellido1;
	}

	public void setApellido1(char apellido1) {
		this.apellido1 = apellido1;
	}

	public char getApellido2() {
		return apellido2;
	}

	public void setApellido2(char apellido2) {
		this.apellido2 = apellido2;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public boolean isSexo() {
		return sexo;
	}

	public void setSexo(boolean sexo) {
		this.sexo = sexo;
	}

	public java.sql.Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(java.sql.Date created_at) {
		this.created_at = created_at;
	}

	public java.sql.Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(java.sql.Date updated_at) {
		this.updated_at = updated_at;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getHorario_id() {
		return horario_id;
	}

	public void setHorario_id(int horario_id) {
		this.horario_id = horario_id;
	}

	public int getEspecialidad_id() {
		return especialidad_id;
	}

	public void setEspecialidad_id(int especialidad_id) {
		this.especialidad_id = especialidad_id;
	}
}
